import React from 'react';
import { captureUserMedia } from '../lib/app-utils';
import RecordRTC from 'recordrtc';
import AudioPlayer from "react-h5-audio-player";
import Utterance from './utterances';
import './component.css';
import {TiMicrophone,TiMediaStop,TiArrowRepeat} from 'react-icons/ti';
import { FilePond, registerPlugin } from 'react-filepond';
import {postRequest,getRequest} from './requestWrapper';
const hasGetUserMedia = !!(navigator.getUserMedia || navigator.webkitGetUserMedia ||
  navigator.mozGetUserMedia || navigator.msGetUserMedia);
var audioVisualizerNormal = { height: '80px', border: '1px solid silver', maxWidth: '600px', width: 'calc(100% - 64px)', margin: 32 };
var audioVisualizerRecording = { height: '80px', border: '1px solid silver', background: "url('https://forum.bubble.is/uploads/default/original/3X/8/5/8507e1871658c63a4b14912068ab4db7f79e7ba5.gif')", backgroundSize: 'contain', maxWidth: '600px', width: 'calc(100% - 64px)', margin: 32 };
class RecordPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      recordAudio: null,
      src: null,
      recordSuccess: false,
      uploading: false,
      recording: false,
      audioVisualizer: audioVisualizerNormal,
      audioVisualizerRecording
    };
    this.requestUserMedia = this.requestUserMedia.bind(this);
    this.startRecord = this.startRecord.bind(this);
    this.stopRecord = this.stopRecord.bind(this);
    this.uploadRecording = this.uploadRecording.bind(this);
    this.resetRecording = this.resetRecording.bind(this);
  }
  componentDidMount() {
    if (!hasGetUserMedia) {
      alert("Your browser cannot stream from your webcam. Please switch to Chrome or Firefox.");
      return;
    }
    this.requestUserMedia();
  }
  requestUserMedia() {
    console.log('requestUserMedia')
    captureUserMedia((stream) => {
      // var binaryStream=[];
      // var data=binaryStream.push(stream);

      // this.setState({ src: `${window.URL.createObjectURL(new Blob(binaryStream)).split("b:")[1]}.webm` });
      // this.audioRef.current.src=this.state.src;
      // this.audioRef.current.play();
      // this.audioRef.current.muted=true;
      // console.log('setting state', this.state)
    });
  }
  startRecord() {
    captureUserMedia((stream) => {
      this.state.recordAudio = RecordRTC(stream, { type: 'audio', mimeType:'audio/wav'});
      this.state.recordAudio.startRecording();
      this.setState({
        recording: true
      });
    });
  }
  stopRecord() {
    this.state.recordAudio.stopRecording(() => {
      this.state.recordAudio.getDataURL((URI) => {
        this.pond.addFile(URI);
        this.setState({
          src: URI,
          recording: false,
          recordSuccess: true
        });
      });
    });
  }
  resetRecording() {
    this.state.recordAudio.reset();
    this.setState({
      src: null
    });
  }
  uploadRecording() {
    // generating random string
    // let params = {
    //   type: 'audio/webm',
    //   data: this.state.recordAudio.blob,
    //   id: Math.floor(Math.random()*90000) + 10000
    // }
    function generateRandomString(name) {
      if (window.crypto) {
        var a = window.crypto.getRandomValues(new Uint32Array(3)),
          token = '';
        for (var i = 0, l = a.length; i < l; i++) token += a[i].toString(36);
        return name + token + '.webm';
      } else {
        return name + (Math.random() * new Date().getTime()).toString(36).replace(/\./g, '') + 'webm';
      }
    }
    if (this.state.recordSuccess) {
      this.setState({ uploading: true });
      var blob = this.state.recordAudio.getBlob();
      var file = new File([blob], generateRandomString('audio'), {
        type: 'audio/wav'
      });
      const formData = new FormData()
      formData.append('file', file);
      request("http://localhost:3005/uploadtest",'POST',formData,keycloak)
      .then(res => res.json())
        .then(response => {
          console.log('---', response);
          this.setState({
            uploading: false,
            uploadSuccess: true
          });
          console.log('Success:', JSON.stringify(response));
          this.setState({
            src: `${response.fileURL}`
          });
        })
        .catch(error => console.error('Error:', error));
    }
    else {
      alert('Please record audio first');
    }
  }
  render() {
    return (
      <div>
        <img src="/static/android-chrome-192x192.png" height="56" style={{
          display: 'block',
          margin: 'auto',
          marginTop: 48,
        }} /><Utterance />
        <div className="audioParent" style={{ width: '100%', margin: 'auto', display: 'flex', flexFlow: 'column', maxWidth: 600 }}>
          <div className="audioVisualizer" style={this.state.audioVisualizer}>
            {
              this.state.src !== null ? 
              <AudioPlayer src={this.state.src} autoPlay></AudioPlayer> :
              <></>
            }
          </div>
          <div className="audioButtons" style={{ display: 'flex', justifyContent: 'center' }}>
            <button style={{ color: 'white', padding: '6px', margin: '10px', backgroundColor: '#000', border:0}} onClick={this.startRecord}><TiMicrophone size={30}/></button>
            <button style={{ color: 'white', padding: '6px', margin: '10px', backgroundColor: 'red', border:0 }} onClick={this.stopRecord}><TiMediaStop size={30}/></button>
            <button style={{ color: 'white', padding: '6px', margin: '10px', backgroundColor: '#286090', border:0}} onClick={this.resetRecording}><TiArrowRepeat size={30}/></button>
          </div>
          {this.state.recording ? <div className="audioVisualizer" style={this.state.audioVisualizerRecording}></div> : <React.Fragment></React.Fragment>}
          {this.state.uploading ? <div className="audioVisualizer" style={this.state.audioVisualizerRecording}></div> : <React.Fragment></React.Fragment>}
        </div>
        <div style={{ maxWidth: 600, margin: 'auto',display:this.state.recordSuccess?'block':'none' }}>
          <FilePond instantUpload={false} allowBrowse={false} allowDrop={false} ref={ref => this.pond = ref} server="/upload" allowMultiple={true} />
          {/* <center>
            <button style={{ color: 'white', padding: '10px', margin: '10px', backgroundColor: '#31b0d5', borderColor: '#31b0d5' }} onClick={this.uploadRecording}>Submit</button>
          </center> */}
        </div>
      </div>
    )
  }
}
export default RecordPage;
// import React from 'react';
//
// export default () => 'Hello ';
