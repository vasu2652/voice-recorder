import React from 'react'
import { Icon, Row, Col, Layout } from 'antd';
import { Tooltip } from 'antd';
import Fab from '@material-ui/core/Fab';
const {
  Content
} = Layout;

export default class extends React.Component {
  constructor(props) {
    super(props);

  }
  componentDidMount() {
    console.log('hello');
  }

  render() {
    const tempUtterances = this.props.UTTERANCES[this.props.selectedIndex];
    return (
      <Row style={{ background: '#fff', padding: 5, minHeight: 380, textAlign: 'center' }} type="flex" justify="center" align="middle">
        <Col xs={4} sm={4} lg={2} onClick={this.props.change(-1)}><Tooltip title="prev"><Fab color="primary" aria-label="prev" >
          <Icon type="left"  />
        </Fab></Tooltip></Col>
        <Col xs={16} sm={16} lg={20} style={{padding:10}}><h5>{tempUtterances && tempUtterances.utterance}</h5></Col>
        <Col xs={4} sm={4} lg={2} onClick={this.props.change(1)}><Tooltip title="next"><Fab color="primary" aria-label="next" >
          <Icon type="right"  />
        </Fab></Tooltip></Col>
      </Row>
      //   <div>
      //   <div style={{
      //     display: 'flex',
      //     maxWidth: 600,
      //     minWidth: 320,
      //     padding: 32,
      //     margin: 'auto',
      //     justifyContent: 'center',
      //   }}>
      //   <Button className="sy-button" style={{
      //     marginRight: 16,
      //   }} >Previous</Button>
      //   <Button className="sy-button" style={{
      //     marginLeft: 16,
      //   }} className="sy-button" onClick={this.onClick(-1)}>Next</Button>
      // </div>
      //   <p style={{
      //     maxWidth: 600,
      //     fontSize: 20,
      //     margin: 'auto',
      //     textAlign: 'center'
      //   }} className="mdl-typography--font-bold ">
      //     {UTTERANCES[this.state.selectedIndex]}
      //   </p>
      // </div>
    )
  }
}
