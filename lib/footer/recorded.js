import React from 'react';
import { Row, Col, Tooltip } from 'antd';
import { keycloak } from '../../pages/_app';
import { FilePond, registerPlugin } from 'react-filepond';
import AudioPlayer from "react-h5-audio-player";
import Fab from '@material-ui/core/Fab';
const { v4 } = require("uuid");
import {TiMicrophone,TiMediaStop,TiArrowRepeat} from 'react-icons/ti';
export default class extends React.Component {
    constructor(props) {
        super(props);
        this.reset=this.reset.bind(this);
        this.state={
            changed:false
        }
        this.test=this.test.bind(this);
        this.server=this.server.bind(this);

    }
    server(props){
        
        return {process: (fieldName, file, metadata, load, error, progress, abort) => {

            // fieldName is the name of the input field
            // file is the actual file object to send

            window.keycloak.loadUserInfo().then(data => {
                const formData = new FormData();
                formData.append(fieldName, file, v4());
                formData.append(window.keycloak.subject, 'yo');
                formData.append(props.currentUtterance._Id, 'yo');
                formData.append(data.name, "yo");
                const request = new XMLHttpRequest();
                request.open('POST', '/upload');

                // Should call the progress method to update the progress to 100% before calling load
                // Setting computable to false switches the loading indicator to infinite mode
                request.upload.onprogress = (e) => {
                    progress(e.lengthComputable, e.loaded, e.total);
                };

                // Should call the load method when done and pass the returned server file id
                // this server file id is then used later on when reverting or restoring a file
                // so your server knows which file to return without exposing that info to the client
                request.onload = ()=> {
                    if (request.status >= 200 && request.status < 300) {
                        // the load method accepts either a string (id) or an object
                        load(request.responseText);
                        var res=request.response.split("'");
                        props.detectRecordedStatus(props.currentUtterance._Id,res[1]);
                    }
                    else {
                        // Can call the error method if something is wrong, should exit after
                        error('oh no');
                    }
                };

                request.send(formData);

                // Should expose an abort method so the request can be cancelled
                return {
                    abort: () => {
                        // This function is entered if the user has tapped the cancel button
                        request.abort();

                        // Let FilePond know the request has been cancelled
                        abort();
                    }
                };

            }).catch(err => console.log(err));
        }
    }

    }
    componentDidMount() {
        console.log("--vasudev--",this.props.URI);
        this.pond.addFile(this.props.URI);
    }
    test(){
        
    }
    reset(){
        this.props.resetRecording();
    }
    render() {
        return (
            <Row type="flex" justify="center" align="middle">
                <Col xs={24} sm={6} lg={8}><audio style={{width:'100%'}}controls>
                <source src={this.props.URI}/>
                </audio></Col>
                <Col xs={24} sm={16} lg={14}>
                    <FilePond style={{width:'100%'}} instantUpload={false} allowBrowse={false} allowDrop={false} ref={ref => this.pond = ref} server={this.server(this.props)} />
                </Col>
                <Col xs={24} sm={2} lg={2}><Tooltip title="Reset"><Fab color="secondary" aria-label="mic" onClick={this.reset} >
                    <TiArrowRepeat size={30}/>
                </Fab></Tooltip></Col>
            </Row>
        )
    }
}