import React from 'react';
import { Row, Col, Button, Tag } from 'antd';
import { keycloak } from '../../pages/_app';
import { FilePond, registerPlugin } from 'react-filepond';
import AudioPlayer from "react-h5-audio-player";
import {TiArrowRepeat} from 'react-icons/ti';
import Fab from '@material-ui/core/Fab';
export default class extends React.Component {
    constructor(props) {
        super(props);
        this.state={
            URI:''
        }
        this.server = {
            process: (fieldName, file, metadata, load, error, progress, abort) => {

                // fieldName is the name of the input field
                // file is the actual file object to send

                window.keycloak.loadUserInfo().then(data => {
                    const formData = new FormData();
                    formData.append(fieldName, file, file.name);
                    formData.append(window.keycloak.subject, 'yo');
                    formData.append(this.props.currentUtterance._Id, 'yo');
                    formData.append(data.name, "yo");
                    const request = new XMLHttpRequest();
                    request.open('POST', '/upload');

                    // Should call the progress method to update the progress to 100% before calling load
                    // Setting computable to false switches the loading indicator to infinite mode
                    request.upload.onprogress = (e) => {
                        progress(e.lengthComputable, e.loaded, e.total);
                    };

                    // Should call the load method when done and pass the returned server file id
                    // this server file id is then used later on when reverting or restoring a file
                    // so your server knows which file to return without exposing that info to the client
                    request.onload = function () {
                        if (request.status >= 200 && request.status < 300) {
                            // the load method accepts either a string (id) or an object
                            load(request.responseText);
                        }
                        else {
                            // Can call the error method if something is wrong, should exit after
                            error('oh no');
                        }
                    };

                    request.send(formData);

                    // Should expose an abort method so the request can be cancelled
                    return {
                        abort: () => {
                            // This function is entered if the user has tapped the cancel button
                            request.abort();

                            // Let FilePond know the request has been cancelled
                            abort();
                        }
                    };

                }).catch(err => console.log(err));
            }
        }

    }
    componentDidMount() {
            this.setState({
                URI:`/media/audio/v1/${this.props.fileName}`
            })
    }
    render() {
        return (
            <Row type="flex" justify="center" align="middle">
                <Col xs={24} sm={24} lg={20}><audio style={{width: '100%'}} controls>
							<source src={this.state.URI} />
						</audio></Col>
                <Col lg={4}>
                <Tag color="green">Accepted</Tag></Col>
            </Row>
        )
    }
}