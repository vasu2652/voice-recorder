import React from 'react';
import { Row, Col,Tooltip } from 'antd';
import Fab from '@material-ui/core/Fab';
import {TiMicrophone,TiMediaStop,TiArrowRepeat} from 'react-icons/ti';
export default class extends React.Component {
    constructor(props) {
        super(props);
        this.record = this.record.bind(this);
    }
    record() {
        this.props.startRecord();
    }
    render() {
        return (
            <Row>
                <Col xs={4} sm={2} lg={8}></Col>
                <Col xs={16} sm={20} lg={8}><Tooltip title="Record"><Fab color="secondary" aria-label="mic" onClick={this.record} >
                    <TiMicrophone size={30}/>
                </Fab></Tooltip></Col>
                <Col xs={4} sm={2} lg={8}></Col>
            </Row>
        )
    }
}