import React from 'react';
import { Row, Col, Button, Tag,Tooltip } from 'antd';
import { keycloak } from '../../pages/_app';
import { FilePond, registerPlugin } from 'react-filepond';
import AudioPlayer from "react-h5-audio-player";
import {TiArrowRepeat,TiDelete} from 'react-icons/ti';
import Fab from '@material-ui/core/Fab';
export default class extends React.Component {
    constructor(props) {
        super(props);
        this.state={
            URI:''
        }
        this.delete=this.delete.bind(this);
        this.server = {
            process: (fieldName, file, metadata, load, error, progress, abort) => {

                // fieldName is the name of the input field
                // file is the actual file object to send

                window.keycloak.loadUserInfo().then(data => {
                    const formData = new FormData();
                    formData.append(fieldName, file, file.name);
                    formData.append(window.keycloak.subject, 'yo');
                    formData.append(this.props.currentUtterance._Id, 'yo');
                    formData.append(data.name, "yo");
                    const request = new XMLHttpRequest();
                    request.open('POST', '/upload');

                    // Should call the progress method to update the progress to 100% before calling load
                    // Setting computable to false switches the loading indicator to infinite mode
                    request.upload.onprogress = (e) => {
                        progress(e.lengthComputable, e.loaded, e.total);
                    };

                    // Should call the load method when done and pass the returned server file id
                    // this server file id is then used later on when reverting or restoring a file
                    // so your server knows which file to return without exposing that info to the client
                    request.onload = function () {
                        if (request.status >= 200 && request.status < 300) {
                            // the load method accepts either a string (id) or an object
                            load(request.responseText);
                        }
                        else {
                            // Can call the error method if something is wrong, should exit after
                            error('oh no');
                        }
                    };

                    request.send(formData);

                    // Should expose an abort method so the request can be cancelled
                    return {
                        abort: () => {
                            // This function is entered if the user has tapped the cancel button
                            request.abort();

                            // Let FilePond know the request has been cancelled
                            abort();
                        }
                    };

                }).catch(err => console.log(err));
            }
        }

    }
    componentDidMount() {
            this.setState({
                URI:`/media/audio/v1/${this.props.fileName}`
            })
            //this.pond.addFile(`/media/audio/v1/${this.props.fieldName}`);
        
    }
    delete(){
        this.props.deleteRecording();
    }
    render() {
        return (
            <Row type="flex" justify="center" align="middle">
                <Col xs={24} sm={16} lg={16}><audio controls>
							<source src={this.state.URI} />
						</audio></Col>
                <Col xs={12} sm={4} lg={4}>
                    <Tag color="red">processing</Tag>
                </Col>
                <Col xs={12} sm={4} lg={4}><Tooltip title="Delete"><Fab color="secondary" aria-label="mic" onClick={this.delete}>
                    <TiDelete size={30}/>
                </Fab></Tooltip></Col>
            </Row>
        )
    }
}