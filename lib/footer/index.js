import React from 'react';
import { Layout} from 'antd';
import Record from './record';
import Recording from './recording';
import Recorded from './recorded';
import RecordAccepted from './recordAccepted';
import InProcess from './inprocess';
const {
    Footer
  } = Layout;
export default class extends React.Component{
    constructor(props){
        super(props);
        this.detectRecordedStatus=this.detectRecordedStatus.bind(this);
    }
    detectRecordedStatus(id,fileName){
        this.props.detectRecordedStatus(id,fileName);
    }
    render(){
        return(
            <Footer style={{ textAlign: 'center'}}>
                {this.props.status==='startRecord'?<Record  startRecord={this.props.startRecord}/>:<></>}
                {this.props.status==='recording'?<Recording  stopRecord={this.props.stopRecord}/>:<></>}
                {this.props.status==='recorded'?<Recorded detectRecordedStatus={this.detectRecordedStatus} currentUtterance={this.props.currentUtterance} URI={this.props.URI} resetRecording={this.props.resetRecording}/>:<></>}
                {this.props.status==='RecordRejected'?<Record  startRecord={this.props.startRecord}/>:<></>}
                {this.props.status==='RecordAccepted'?<RecordAccepted currentUtterance={this.props.currentUtterance} URI={this.props.URI} fileName={this.props.fileName}  status="RecordAccepted"/>:<></>}
                {this.props.status==='inProcess'?<InProcess deleteRecording={this.props.deleteRecording} currentUtterance={this.props.currentUtterance} URI={this.props.URI} fileName={this.props.fileName}  status="inProcess" />:<></>}
            </Footer>
        )
    }
}
