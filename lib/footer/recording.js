import React from 'react';
import { Row, Col ,Tooltip} from 'antd';
import Fab from '@material-ui/core/Fab';
import {TiMicrophone,TiMediaStop,TiArrowRepeat} from 'react-icons/ti';
export default class extends React.Component {
    constructor(props) {
        super(props);
        this.stopRecord=this.stopRecord.bind(this);
    }
    stopRecord(){
        this.props.stopRecord();
    }
    render() {
        return (
            <Row type="flex" justify="end" align="middle">
                <Col xs={18} sm={18} lg={19}>
                    <div className="audioVisualizer" style={{ height: 80, background: "url('https://forum.bubble.is/uploads/default/original/3X/8/5/8507e1871658c63a4b14912068ab4db7f79e7ba5.gif')", backgroundSize: 'contain' }}>
                    </div>
                </Col>
                <Col xs={2} sm={2} lg={1}></Col>
                <Col xs={4} sm={4} lg={4}>
                <Tooltip title="Stop"><Fab color="secondary" aria-label="mic" onClick={this.stopRecord} >
                        <TiMediaStop size={30}/>
                    </Fab></Tooltip>
                </Col>
            </Row>
        )
    }
}