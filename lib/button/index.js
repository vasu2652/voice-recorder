import React from 'react'
import Ink from 'react-ink'

const variations = {

}

export default class B extends React.Component {

  // onClick = () => {
  //   this.buttonRipple.activate();
  //   if(this.props.onClick) {
  //     this.props.onClick(e);
  //   }
  // }
  constructor(props) {
    super(props);
    this.state = {
      clicked: 1,
    }
  }
  onClick = (e) => {
    typeof this.props.onClick === 'function' && this.props.onClick(e);
  }
  render() {
    return (
      <button onClick={this.onClick} className={`sy-button ${this.props.className}`} style={this.props.style || {}}>
        <Ink />
        {this.props.children}
      </button>
    )
  }
}
