export const postRequest = (url, method, body, keycloak) => {
    return fetch(url, {
        method: method,
        headers: {
            ...headers,
            "Authorization": 'Bearer ' + keycloak.token
        },
        body: body
    });
}
export const getRequest=()=>{
    
}