import React from "react";
import { Table, Card, Button } from "antd";
import { Menu, Icon, Select, Layout, Tag } from "antd";
import "./component.css";
import { keycloak } from "../pages/_app";
import { Header } from './header';
//import Test from './test';

export default class extends React.Component {
	state = {
		list: []
	};
	componentDidMount() {
		console.log(keycloak);
		console.log(window.keykloak);
		fetch("/getSpingeList")
			.then(d => d.json())
			.then(d => {
				if (d.data) {
					this.setState({
						list: d.data
					});
				}
			});
	}
	logout() {
		this.props.logout()
	}
	approve = (id, accept) => e => {
		window.keycloak.loadUserInfo().then(d => {
			const userid = window.keycloak.subject;
			const name = d.name;
			fetch(
				`/updateAcceptance/${id}/${
				accept ? "accept" : "reject"
				}/${userid}/${name}`
			)
				.then(d => d.json())
				.then(res => {
					this.setState({
						list: this.state.list.map(d =>
							id === d._id
								? {
									...d,
									...res.data
								}
								: d
						)
					});
				});
		});
	};
	render() {
		const columns = [
			{
				title: "File Path",
				dataIndex: "file_path",
				key: "file_path",
				render: path => {
					return (
						<audio controls>
							<source src={`/media/audio/v1/${path}`}></source>
						</audio>
					);
				}
			},
			// {
			// 	title: "Duration",
			// 	dataIndex: "file_duration",
			// 	key: "file_duration"
			// },
			{
				title: "Recorded By",
				dataIndex: "recorded_by",
				key: "recorded_by"
			},
			{ title: "Judged By", dataIndex: "judged_by", key: "judged_by" },
			{
				title: "Judged On",
				dataIndex: "judged_on",
				key: "judged_on",
				render: (data) => <>{data && data.split('T')[0]}</>
			},
			{
				title: "Status",
				dataIndex: "accepted",
				key: "accepted",
				render: (accepted, record) => {
					// return <Tag color="green">Approved</Tag>
					if (accepted === null) {
						return (
							<div>
								<Button
									type="primary"
									onClick={this.approve(record._id, true)}
								>
									<Icon type="check" />
								</Button>
								<Button
									type="danger"
									onClick={this.approve(record._id, false)}
								>
									<Icon type="close" />
								</Button>
							</div>
						);
					}
					return (
						<Tag color={accepted ? "green" : "red"}>
							{accepted ? "Accepted" : "Rejected"}
						</Tag>
					);
				}
			}
		];
		const data = this.state.list.map(d => ({
			...d,
			// accepted: true,
			key: d._id
		}));
		return (
			<div>
				<Header role={window.keycloak.hasResourceRole('judge')} logout={this.logout} />
				<Card style={{ paddingTop: 100 }}>
					<h2>Spinge List</h2>
					<Table
						style={{ maxWidth: 1000, margin: "auto" }}
						columns={columns}
						dataSource={data}
						onRow={(record, rowIndex) => {
							return {
								onClick: (event) => {
									console.log(record,rowIndex)
								}
							}
						}}
					/>
				</Card>
			</div>
		);
	}
}
