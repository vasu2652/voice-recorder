import React from "react";
import { Layout, Menu, Icon, Select, Row, Col, Breadcrumb } from "antd";
import { keycloak } from "../pages/_app";

//import { captureUserMedia } from "../lib/app-utils";
//import RecordRTC from "recordrtc";
import Utterance from "./layoutUtterances";
import "./component.css";
import Footer from "./footer/index";
import { Header } from "./header";
import RecorderService from "./recorderFiles/crossBrowserRecorder";
// const hasGetUserMedia = !!(
//   navigator.getUserMedia ||
//   navigator.webkitGetUserMedia ||
//   navigator.mozGetUserMedia ||
//   navigator.msGetUserMedia
// );
// var audioVisualizerNormal = { height: '80px', border: '1px solid silver', maxWidth: '600px', width: 'calc(100% - 64px)', margin: 32 };
// var audioVisualizerRecording = { height: '80px', border: '1px solid silver', background: "url('https://forum.bubble.is/uploads/default/original/3X/8/5/8507e1871658c63a4b14912068ab4db7f79e7ba5.gif')", backgroundSize: 'contain', maxWidth: '600px', width: 'calc(100% - 64px)', margin: 32 };

const { Content } = Layout;

//const Option = Select.Option;
class CrossLayoutDemo extends React.Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.state = {
      recordAudio: null,
      UTTERANCES: [],
      src: null,
      selectedIndex: 0
    };
    this.recorderSrvc = new RecorderService();
    this.recorderSrvc.em.addEventListener("recording", evt =>
      this.onNewRecording(evt)
    );
//    this.requestUserMedia = this.requestUserMedia.bind(this);
    this.startRecord = this.startRecord.bind(this);
    this.stopRecord = this.stopRecord.bind(this);
    this.logout = this.logout.bind(this);
    //this.uploadRecording = this.uploadRecording.bind(this);
    this.resetRecording = this.resetRecording.bind(this);
    this.deleteRecording = this.deleteRecording.bind(this);
    this.detectRecordedStatus = this.detectRecordedStatus.bind(this);
  }
  componentDidMount() {
    
    fetch(`/getutterancesandrecordings/${window.keycloak.subject}`,{
      method: 'POST',
    }).then(data => data.json())
      .then(d => {
        console.log(d);
        this.setState({
          UTTERANCES: d.utterances.map(item => {
            for (let recording of d.recordings) {
              if (recording.utterance_id === item._Id) {
                if (recording.accepted === true) {
                  item.status = "RecordAccepted";
                  item.fileName = recording.file_path;
                  return item;
                } else if (recording.accepted === false) {
                  item.status = "RecordRejected";
                  return item;
                } else if (recording.deleted) {
                  item.status = "startRecord";
                  return item;
                } else {
                  item.status = "inProcess";
                  item.fileName = recording.file_path;
                  return item;
                }
              }
            }
            item.status = "startRecord";
            return item;
          })
        });
        console.log(this.state.UTTERANCES);
      });
    //this.requestUserMedia();
  }
  logout() {
    window.keycloak.logout();
  }
  // requestUserMedia() {
  //   console.log("requestUserMedia");
  //   captureUserMedia(stream => {
  //     // var binaryStream=[];
  //     // var data=binaryStream.push(stream);
  //     // this.setState({ src: `${window.URL.createObjectURL(new Blob(binaryStream)).split("b:")[1]}.webm` });
  //     // this.audioRef.current.src=this.state.src;
  //     // this.audioRef.current.play();
  //     // this.audioRef.current.muted=true;
  //     // console.log('setting state', this.state)
  //   });
  // }

  startRecord() {
    console.log("start recording");
    this.recorderSrvc.config.stopTracksAndCloseCtxWhenFinished = true;
    this.recorderSrvc.config.createDynamicsCompressorNode = false;
    this.recorderSrvc.config.enableEchoCancellation = true;
    this.recorderSrvc
      .startRecording()
      .then(() => {
        this.setState({
          UTTERANCES: this.state.UTTERANCES.map((utterance, index) => {
            if (index === this.state.selectedIndex) {
              return { ...utterance, status: "recording", src: null };
            }
            return utterance;
          })
        });
      })
      .catch(error => {
        console.error("Exception while start recording: " + error);
        alert("Exception while start recording: " + error.message);
      });
  }
  stopRecord() {
    this.recorderSrvc.stopRecording();
  }
  onNewRecording(evt) {
    this.setState({
      src: evt.detail.recording.blobUrl,
      UTTERANCES: this.state.UTTERANCES.map((utterance, index) => {
        if (index === this.state.selectedIndex) {
          return {
            ...utterance,
            status: "recorded",
            src: evt.detail.recording.blobUrl
          };
        }
        return utterance;
      })
    });
  }
  // stopRecord() {
  //   console.log('I have been called');
  //   this.state.recordAudio.stopRecording(() => {
  //     this.state.recordAudio.getDataURL((URI) => {
  //       this.setState({
  //         src: URI,
  //         UTTERANCES: this.state.UTTERANCES.map((utterance, index) => {
  //           if (index === this.state.selectedIndex) {
  //             return { ...utterance, status: 'recorded', src: URI }
  //           }
  //           return utterance;
  //         })
  //       });
  //     });
  //   });
  // }

  //Needs to modify in the source code of crossBrowserRecorder.js

  resetRecording() {
    this.setState({
      src: "",
      UTTERANCES: this.state.UTTERANCES.map((utterance, index) => {
        if (index === this.state.selectedIndex) {
          return { ...utterance, status: "startRecord", src: "" };
        }
        return utterance;
      })
    });
  }
  deleteRecording() {
    //this.state.recordAudio.reset();
    const tempUtterances = this.state.UTTERANCES[this.state.selectedIndex];
    fetch(`/deleterecording/${window.keycloak.subject}/${tempUtterances._Id}`,{
      method: 'POST',
    })
      .then(data => data.json())
      .then(d => {
        console.log(d);
        this.setState({
          UTTERANCES: d.utterances.map(item => {
            for (let recording of d.recordings) {
              if (recording.utterance_id === item._Id) {
                if (recording.accepted === true) {
                  item.status = "RecordAccepted";
                  item.fileName = recording.file_path;
                  return item;
                } else if (recording.accepted === false) {
                  item.status = "RecordRejected";

                  return item;
                } else if (recording.deleted) {
                  item.status = "startRecord";
                  return item;
                } else {
                  item.status = "inProcess";
                  item.fileName = recording.file_path;
                  return item;
                }
              }
            }
            item.status = "startRecord";
            return item;
          })
        });
        console.log("--", this.state.UTTERANCES);
      });
  }
  detectRecordedStatus(id, fileName) {
    this.setState({
      UTTERANCES: this.state.UTTERANCES.map(item => {
        if (item._Id === id) {
          item.status = "inProcess";
          item.fileName = fileName;
          return item;
        }
        return item;
      })
    });
  }
  utteranceChange = val => e => {
    e.preventDefault();
    const tempUtterances = this.state.UTTERANCES[this.state.selectedIndex];
    if (tempUtterances.status === "recording") {
      val = 0;
      return;
    } else {
      this.setState({
        selectedIndex:
          this.state.selectedIndex + val < 0
            ? (this.state.UTTERANCES.length +
                (this.state.selectedIndex + val)) %
              this.state.UTTERANCES.length
            : (this.state.selectedIndex + val) % this.state.UTTERANCES.length
      });
      return;
    }
  };
  handleChange = value => {
    console.log(`selected ${value}`);
  };
  render() {
    const tempUtterances = this.state.UTTERANCES[this.state.selectedIndex];
    return (
      <Layout>
        <Header role={window.keycloak.hasResourceRole("judge")} logout={this.logout} />
        <Content style={{ marginTop: 64 }}>
          {/* <Breadcrumb style={{ margin: '16px 0' }}>
            <Breadcrumb.Item>Home</Breadcrumb.Item>
            <Breadcrumb.Item>List</Breadcrumb.Item>
            <Breadcrumb.Item>App</Breadcrumb.Item>
          </Breadcrumb> */}
          <Utterance
            selectedIndex={this.state.selectedIndex}
            change={this.utteranceChange}
            UTTERANCES={this.state.UTTERANCES}
          />
        </Content>
        <Footer
          detectRecordedStatus={this.detectRecordedStatus}
          URI={this.state.src}
          fileName={tempUtterances && tempUtterances.fileName}
          status={tempUtterances && tempUtterances.status}
          currentUtterance={tempUtterances}
          startRecord={this.startRecord}
          stopRecord={this.stopRecord}
          resetRecording={this.resetRecording}
          deleteRecording={this.deleteRecording}
        />
      </Layout>
    );
  }
}
export default CrossLayoutDemo;
// uploadRecording() {
//   // generating random string
//   // let params = {
//   //   type: 'audio/webm',
//   //   data: this.state.recordAudio.blob,
//   //   id: Math.floor(Math.random()*90000) + 10000
//   // }
//   function generateRandomString(name) {
//     if (window.crypto) {
//       var a = window.crypto.getRandomValues(new Uint32Array(3)),
//         token = '';
//       for (var i = 0, l = a.length; i < l; i++) token += a[i].toString(36);
//       return name + token + '.webm';
//     } else {
//       return name + (Math.random() * new Date().getTime()).toString(36).replace(/\./g, '') + 'webm';
//     }
//   }
//   if (this.state.recordSuccess) {
//     this.setState({ uploading: true });
//     var blob = this.state.recordAudio.getBlob();
//     var file = new File([blob], generateRandomString('audio'), {
//       type: 'audio/wav'
//     });
//     const formData = new FormData()
//     formData.append('file', file);
//     request("http://localhost:3005/uploadtest", 'POST', formData, keycloak)
//       .then(res => res.json())
//       .then(response => {
//         console.log('---', response);
//         this.setState({
//           uploading: false,
//           uploadSuccess: true
//         });
//         console.log('Success:', JSON.stringify(response));
//         this.setState({
//           src: `${response.fileURL}`
//         });
//       })
//       .catch(error => console.error('Error:', error));
//   }
//   else {
//     alert('Please record audio first');
//   }
// }
