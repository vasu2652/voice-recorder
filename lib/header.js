import React from "react";
import { Menu, Icon, Select, Layout, Tag } from "antd";
import {keycloak} from '../pages/_app';
import Link from 'next/link'
function handleChang(value){
    console.log(`selected ${value}`);
	}
	
export const Header = (props) => {
	console.log(props.role);
	function logout(){
		props.logout();
	}
	console.log("---",props);
	return <Layout.Header
		theme="light"
		collapsible="true"
		style={{ position: "fixed", zIndex: 1, width: "100%" }}
	>
		<Menu
			theme="light"
			mode="horizontal"
			defaultSelectedKeys={["1"]}
			style={{ lineHeight: "64px" }}
		>
			<Menu.Item key="1">
				<Link href="/">
					<img src="/static/android-chrome-192x192.png" height="56" />	
				</Link>
			</Menu.Item>
			{props.role?<Menu.Item key="2">
				<Link href="/list">
					List
				</Link>
			</Menu.Item>:<></>}
			<Menu.Item key="3">
				<Select
					defaultValue="tamil"
					style={{ width: 120 }}
					//  onChange={handleChange}
				>
					<Select.Option value="telugu">telugu</Select.Option>
					<Select.Option value="tamil">tamil</Select.Option>
					<Select.Option value="hindi">hindi</Select.Option>
					<Select.Option value="malyalam">malyalam</Select.Option>
				</Select>
			</Menu.Item>
			<Menu.Item key="4" style={{ float: "right" }} onClick={logout}>
				<Icon type="logout" />
				Logout
			</Menu.Item>
		</Menu>
	</Layout.Header>;
};
