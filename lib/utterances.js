import React from 'react'
import Button from './button';
import {TiArrowBack, TiArrowForward} from 'react-icons/ti';
const UTTERANCES = [
  'The Best way to predict the future is to design it!',
  'The Best way to predict the future is to design it again!',
  'The Best way to predict the future is to design it again and again!'
]

export default class extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
    selectedIndex: 0,
    }
  }
  onClick = (val) => (e) => {
    e.preventDefault();
    this.setState({
      selectedIndex: this.state.selectedIndex+val < 0 ?
      (UTTERANCES.length + (this.state.selectedIndex+val))%UTTERANCES.length : ((this.state.selectedIndex+val)%UTTERANCES.length)
    })

  }
  render() {
    return(
      <div>
      <div style={{
        display: 'flex',
        maxWidth: 600,
        minWidth: 320,
        padding: 32,
        margin: 'auto',
        justifyContent: 'center',
      }}>
      <Button className="sy-button" style={{
        marginRight: 16,
      }} onClick={this.onClick(-1)}>Previous</Button>
      <Button className="sy-button" style={{
        marginLeft: 16,
      }} className="sy-button" onClick={this.onClick(-1)}>Next</Button>
    </div>
      <p style={{
        maxWidth: 600,
        fontSize: 20,
        margin: 'auto',
        textAlign: 'center'
      }} className="mdl-typography--font-bold ">
        {UTTERANCES[this.state.selectedIndex]}
      </p>
    </div>
    )
  }
}
