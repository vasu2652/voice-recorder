const sass = require('@zeit/next-sass')
// next.config.js
const css = require('@zeit/next-css')
const less = require('@zeit/next-less')
const withPlugins = require('next-compose-plugins');

module.exports = withPlugins([
  [sass],
  [css],
  [less]
]);


const isProd = process.env.NODE_ENV === 'production'

// fix: prevents error when .less files are required by node
if (typeof require !== 'undefined') {
  require.extensions['.less'] = file => { }
}

module.exports = less(sass(css({
  lessLoaderOptions: {
    javascriptEnabled: true
  }
})))