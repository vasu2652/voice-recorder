#!/bin/bash
docker stop zen3-voice-recorder
docker rm zen3-voice-recorder
docker build -t zen3-voice-recorder .
docker run --name=zen3-voice-recorder -d --restart unless-stopped --net=host zen3-voice-recorder
