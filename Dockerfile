FROM node:carbon

# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./

RUN npm install
# If you are building your code for production
# RUN npm install --only=production

# Bundle app source
COPY . .

# Build to DIST Directory
RUN npm run build
RUN npm prune --production

# RUN apt-get update && apt-get install -y \
#     iputils-ping \
#     iproute2 \
#     curl \
#  && rm -rf /var/lib/apt/lists/*

# COPY docker-entrypoint.sh /

# ENTRYPOINT ["/docker-entrypoint.sh"]

EXPOSE 3005
CMD [ "node", "server.js"]
