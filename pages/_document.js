import Document, { Head, Main, NextScript } from 'next/document'

const fontProps = {
  font: "Roboto"
}
export const Font = (props = fontProps) => (
  <React.Fragment>
    <style>
      {`
        html, * {
          font-family: ${props.font}, sans-serif;
        }
      `}
    </style>
    <link href={`https://fonts.googleapis.com/css?family=${props.font}:400,500,700`} rel="stylesheet" />
  </React.Fragment>
)


export default class MyDocument extends Document {
  static async getInitialProps(ctx) {
    const initialProps = await Document.getInitialProps(ctx)
    return { ...initialProps }
  }
  componentDidMount() {

  }

  render() {
    return (
      <html lang="en">
        <Head>
          <style>{`body {
          margin: 0;
        }`}</style>
          <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" />

          <meta name="viewport" content="width=device-width, initial-scale=1.0"></meta>
          <script src="https://apps.sayint.ai/auth/js/keycloak.js"></script>
        </Head>
        <body>
          <Font {...fontProps} />
          <Main />
          <NextScript />

        </body>
      </html>
    )
  }
}
