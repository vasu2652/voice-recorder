import dynamic from 'next/dynamic'
import '../styles/index.scss';
// Import FilePond styles
import 'filepond/dist/filepond.min.css';
import Head from '../lib/head';

const DynamicComponentWithNoSSR = dynamic(() => import('../lib/layout'), {
  ssr: false
})

function test() {
  return (
    <div className="mdc-typography">
      <Head />
      <DynamicComponentWithNoSSR />
    </div>
  )
}

export default test
