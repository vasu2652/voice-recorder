import React from "react";
import App, { Container } from "next/app";
import "../css/antd.less";
//import '../i18n';

export let keycloak;

export default class MyApp extends App {
  constructor(props) {
    super(props)
    this.state = {
      auth: false
    };
  }
  static async getInitialProps({ Component, router, ctx }) {
    let pageProps = {};

    if (Component.getInitialProps) {
      pageProps = await Component.getInitialProps(ctx);
    }

    return { pageProps };
  }
  componentDidMount() {
    window.keycloak = window.Keycloak({
      url: "/auth",
      realm: "spinge",
      clientId: "sayint-spinge"
    });
    window.keycloak
      .init({ onLoad: "check-sso", checkLoginIframe: false })
      .success(authenticated => {
        this.setState({
          auth: authenticated
        });
        if (!authenticated) {
          window.keycloak.login();
        }
        console.log("hello", window.keycloak.subject);
        // window.user = {
        // 	username: "Demo"
        // };
      })
      .error(function(e) {
        window.keycloak.login();
      });
  }
  componentWillUnmount() {}

  render() {
    const { Component, pageProps } = this.props;
    return (
      <Container>
        {this.state.auth ? <Component {...pageProps} /> : <></>}
      </Container>
    );
  }
}
