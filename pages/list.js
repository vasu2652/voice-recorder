import dynamic from 'next/dynamic'


const DynamicComponentWithNoSSR = dynamic(() => import('../lib/listComp'), {
  ssr: false
})

function list(props) {
  
  return (
    <div className="mdc-typography">
      <DynamicComponentWithNoSSR />
    </div>
  )
}
export default list;
