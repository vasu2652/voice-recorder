import dynamic from 'next/dynamic'
import '../styles/index.scss';
// Import FilePond styles
import 'filepond/dist/filepond.min.css';
import Head from '../lib/head';

const DynamicComponentWithNoSSR = dynamic(() => import('../lib/crossLayout'), {
  ssr: false
})

function index() {
  return (
    <div className="mdc-typography">
      <Head />
      <DynamicComponentWithNoSSR />
    </div>
  )
}

export default index
