import dynamic from 'next/dynamic'


const DynamicComponentWithNoSSR = dynamic(() => import('../lib/component'), {
  ssr: false
})

function home(props) {
  
  return (
    <div className="mdc-typography">
      <DynamicComponentWithNoSSR />
    </div>
  )
}
export default home
