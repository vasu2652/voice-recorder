const db=require('./config/db');
module.exports = (app) => {
  app.post('/getutterancesandrecordings/:userid', function(req, res) {
      var userid=req.params.userid;
        db.any('SELECT * FROM utterances.items ORDER BY "_Id" ASC').then(utterances=>{
            db.any(`SELECT * FROM utterances.recordings WHERE recorded_by_id='${userid}' ORDER BY "_id" ASC`).then(recordings=>{
                res.send({
                    utterances,
                    recordings
                });
            }).catch(err=>console.log(err));
            
        }).catch(err=>{
            res.send({error:err});
        })
    });
    app.post('/deleterecording/:userid/:utteranceid',function(req,res){
        var userid=req.params.userid;
        var utteranceid=req.params.utteranceid;
        db.any(`UPDATE utterances.recordings
        SET "deleted"=true
        WHERE "utterance_id"=$1 AND recorded_by_id=$2 RETURNING *`,[utteranceid,userid]).then(d=>{
            res.redirect(307,`/getutterancesandrecordings/${userid}`)
        }).catch(err=>console.log(err));
    });
}
