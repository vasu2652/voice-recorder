const promise = require('bluebird'); // or any other Promise/A+ compatible library;
const {v4}= require('uuid');
const initOptions = {
    promiseLib: promise // overriding the default (ES6 Promise);
};

const pgp = require('pg-promise')(initOptions);
// See also: http://vitaly-t.github.io/pg-promise/module-pg-promise.html

// Database connection details;
const cn = {
    host: 'apps.sayint.ai', // 'localhost' is the default;
    port: 5432, // 5432 is the default;
    database: 'data_services',
    user: 'alpha',
    password: 'alpha123'
};
// You can check for all default values in:
// https://github.com/brianc/node-postgres/blob/master/lib/defaults.js

const db = pgp(cn);
// const UTTERANCES = [
//     'The Best way to predict the future is to design it!',
//     'The Best way to predict the future is to design it again!',
//     'The Best way to predict the future is to design it again and again!'
// ];
// UTTERANCES.map(item=>{
//     db.one('INSERT INTO utterances.items("_Id", language, utterance) VALUES($1, $2, $3) RETURNING "_Id"', [v4(),'english', item]).then(res => {
//         console.log(res);
//     }).catch((e) => {
//         console.log(e);
//     });
// });
// database instance;
module.exports=db;