// var s3 = require('s3');

// var client = s3.createClient({
//   maxAsyncS3: 20,     // this is the default
//   s3RetryCount: 3,    // this is the default
//   s3RetryDelay: 1000, // this is the default
//   multipartUploadThreshold: 20971520, // this is the default (20 MB)
//   multipartUploadSize: 15728640, // this is the default (15 MB)
//   s3Options: {
//     accessKeyId: "AKIAI2BSMMUTBBLMQBPA",
//   secretAccessKey: "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",
//     // any other options are passed to new AWS.S3()
//     // See: http://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/Config.html#constructor-property
//   },
// });
var AWS = require("aws-sdk");
var zen3Bucket = require("./bucket");
AWS.config.update({
	accessKeyId: "AKIAI2BSMMUTBBLMQBPA",
	secretAccessKey: "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",
	region: "us-east-1"
});
var s3 = new AWS.S3();
const db = require("./config/db");
module.exports = app => {
	app.get("/getlist", function(req, res) {
		var params = {
			Bucket: "zen3"
		};
		s3.listObjects(params, function(err, data) {
			if (err) console.log(err, err.stack);
			// an error occurred
			else res.send(data); // successful response
		});
	});
	app.get("/getSpingeList", (req, res) => {
		db.any("SELECT * FROM utterances.recordings WHERE deleted = false")
			.then(function(data) {
				res.send({ data });
			})
			.catch(function(error) {
				res.send({ error: error.message });
			});
	});
	app.get("/updateAcceptance/:id/:acceptance/:userid/:username", (req, res) => {
		const acceptance = req.params["acceptance"];
    const id = req.params["id"];
    const userid = req.params["userid"];
    const username = req.params["username"];
		db.one(
			"UPDATE utterances.recordings SET accepted=$1, judged_by=$2, judged_by_id=$3, judged_on=NOW() WHERE _id=$4 RETURNING *",
			[acceptance === "accept", username, userid , id]
		)
			.then(d => {
				res.send({
					data: d
				});
			})
			.catch(e => res.send({ error: e.message }));
	});
	app.get("/media/audio/v1/:id", (req, res) => {
		const file = req.params["id"];
		const url = zen3Bucket.getUrl("GET", file, "zen3", 100);
		res.redirect(302, url);
	});
};
