// This file doesn't go through babel or webpack transformation.
// Make sure the syntax and sources this file requires are compatible with the current node version you are running
// See https://github.com/zeit/next.js/issues/1245 for discussions on Universal Webpack or universal Babel
const { createServer } = require("http");
const { parse } = require("url");
const next = require("next");
const attachHandlers = require("./s3-upload");
const getList=require('./attachGetObjects');
const crud=require('./crud');
const dev = false;
const app = next({ dev });
const handle = app.getRequestHandler();
const fs = require("fs");
const express = require("express");
const httpServer = express();
app.prepare().then(() => {
  attachHandlers(httpServer);
  getList(httpServer);
  crud(httpServer);
  httpServer.get("*", (req, res) => {
    handle(req, res)
  }).listen(process.env.PORT || 3005, err => {
			if (err) throw err;
			console.log("> Ready on http://localhost:3005");
		});
});
